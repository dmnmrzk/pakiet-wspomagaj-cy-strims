// Generated by CoffeeScript 1.6.3
(function() {
  var $, bindkeys, shorturl;

  $ = jQuery;

  shorturl = function() {
    var match, url;
    url = $(location).attr('href');
    match = /[t|w|k]\/[A-Za-z0-9]{5,6}/i.exec(url);
    if (match === null) {
      match = "";
    }
    return "http://stri.ms/" + match;
  };

  bindkeys = function() {
    var f;
    $(document).bind('keydown.Shift_a', f = function() {
      return window.location = 'http://strims.pl/s/subskrybowane';
    });
    $(document).bind('keydown.Shift_z', f = function() {
      return window.location = 'http://strims.pl/s/glowny';
    });
    $(document).bind('keydown.Shift_m', f = function() {
      return window.location = 'http://strims.pl/s/moderowane';
    });
    $(document).bind('keydown.Shift_l', f = function() {
      return window.location = 'http://strims.pl/s/losowy';
    });
    $(document).bind('keydown.Shift_w', f = function() {
      return window.location = 'http://strims.pl/s/wszystkie';
    });
    $(document).bind('keydown.Shift_q', f = function() {
      return window.location = 'http://strims.pl/u/subskrybowani';
    });
    $(document).bind('keydown.Shift_x', f = function() {
      return window.location = 'http://strims.pl/s/Glowny/wpisy';
    });
    return $(document).bind('keydown.Shift_c', f = function() {
      return window.location = 'http://strims.pl/wpisy';
    });
  };

  $(function() {
    bindkeys();
    $('#search_right_box').after('<div id="shorturl">Shorturl: <input type="text" value="' + shorturl() + '" readonly="readonly"></div>');
  });

}).call(this);
