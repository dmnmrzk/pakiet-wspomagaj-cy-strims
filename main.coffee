$ = jQuery
shorturl = ->
	url = $(location).attr 'href'
	match = /[t|w|k]\/[A-Za-z0-9]{5,6}/i.exec url
	if match == null
		match = ""
	return "http://stri.ms/#{match}"

bindkeys = ->
	$(document).bind('keydown.Shift_a', f = -> window.location = 'http://strims.pl/s/subskrybowane')
	$(document).bind('keydown.Shift_z', f = -> window.location = 'http://strims.pl/s/glowny')
	$(document).bind('keydown.Shift_m', f = -> window.location = 'http://strims.pl/s/moderowane')
	$(document).bind('keydown.Shift_l', f = -> window.location = 'http://strims.pl/s/losowy')
	$(document).bind('keydown.Shift_w', f = -> window.location = 'http://strims.pl/s/wszystkie')
	$(document).bind('keydown.Shift_q', f = -> window.location = 'http://strims.pl/u/subskrybowani')
	$(document).bind('keydown.Shift_x', f = -> window.location = 'http://strims.pl/s/Glowny/wpisy')
	$(document).bind('keydown.Shift_c', f = -> window.location = 'http://strims.pl/wpisy')

$ ->
	bindkeys()
	$('#search_right_box').after '<div id="shorturl">Shorturl: <input type="text" value="'+shorturl()+'" readonly="readonly"></div>'
	return